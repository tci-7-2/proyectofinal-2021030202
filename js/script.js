var carrito = [];
var total = 0;

function agregarAlCarrito(id, producto, precio) {
    carrito.push({ id, producto, precio });
    total += precio;
    actualizarCarrito();
}

function actualizarCarrito() {
    var carritoElement = document.getElementById("carrito");
    var totalElement = document.getElementById("total");

    // Limpiar la lista de productos en el carrito
    carritoElement.innerHTML = "";

    // Crear nuevos elementos de lista para cada producto en el carrito
    carrito.forEach(item => {
        var li = document.createElement("li");
        li.textContent = `${item.producto} - $${item.precio.toFixed(2)}`;
        carritoElement.appendChild(li);
    });

    // Actualizar el total
    totalElement.textContent = total.toFixed(2);
}

function realizarCompra() {
    var nombre = document.getElementById("nombre").value;
    var email = document.getElementById("email").value;

    // Enviar la información del carrito y del cliente al back-end para procesar la compra
    fetch('/realizarCompra', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ nombre, email, carrito }),
    })
    .then(response => response.json())
    .then(data => {
        alert(data.message);
        // Puedes redirigir o realizar otras acciones después de la compra
    })
    .catch((error) => {
        console.error('Error:', error);
    });
}