import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { getStorage, ref, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDBWZKIX7ZDTmPeSDsTNg3Dt96GfZwc_LA",
    authDomain: "proyectofinal-2021030202.firebaseapp.com",
    databaseURL: "https://proyectofinal-2021030202-default-rtdb.firebaseio.com/",
    projectId: "proyectofinal-2021030202",
    storageBucket: "proyectofinal-2021030202.appspot.com",
    messagingSenderId: "713783532296",
    appId: "1:713783532296:web:f448a3b73c0ca90f5f2abd"
  };

 // Initialize Firebase
 export const app = initializeApp(firebaseConfig);
 export const auth = getAuth(app);
 export const db = getDatabase(app);
 export const gSt = getStorage(app);
 
 const storageRef = ref(gSt);
 export function upload(file,fileName){
   const childRef = ref(storageRef,'img/'+fileName)
   uploadBytes(childRef,file).then((snapshot) => console.log('uploaded'))
   return childRef; 
}