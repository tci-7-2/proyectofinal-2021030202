// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, onValue, ref } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDBWZKIX7ZDTmPeSDsTNg3Dt96GfZwc_LA",
    authDomain: "proyectofinal-2021030202.firebaseapp.com",
    databaseURL: "https://proyectofinal-2021030202-default-rtdb.firebaseio.com/",
    projectId: "proyectofinal-2021030202",
    storageBucket: "proyectofinal-2021030202.appspot.com",
    messagingSenderId: "713783532296",
    appId: "1:713783532296:web:f448a3b73c0ca90f5f2abd"
  };

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

// Función para mostrar productos en la tienda.html
function mostrarProductosHTML() {
    const dbRef = ref(db, 'productos');
    const section = document.getElementById('productos-container');

    onValue(dbRef, (snapshot) => {
        section.innerHTML = ''; // Limpiar contenido anterior

        snapshot.forEach((childSnapshot) => {
            const data = childSnapshot.val();
            agregarProductoAlHTML(data);
        });
    });
}

// Función para agregar un producto al contenedor de productos en la tienda.html
function agregarProductoAlHTML(data) {
    const section = document.getElementById('productos-container');

    const productDiv = document.createElement('div');
    productDiv.className = 'product';

    const productImage = document.createElement('img');
    productImage.src = data.urlImagen;
    productImage.alt = '';
    productDiv.appendChild(productImage);

    const productTitle = document.createElement('h3');
    productTitle.className = 'product-title';
    productTitle.textContent = data.nombre;
    productDiv.appendChild(productTitle);

    const productPrice = document.createElement('p');
    productPrice.className = 'product-price';
    productPrice.textContent = `$${data.precio}`;
    productDiv.appendChild(productPrice);

    const productStock = document.createElement('p');
    productStock.className = 'product-stock';
    productStock.textContent = `Disponible: ${data.cantidad} unidades`;
    productDiv.appendChild(productStock);

    const addToCartBtn = document.createElement('button');
    addToCartBtn.className = 'add-to-cart';
    addToCartBtn.textContent = 'Agregar al carrito';
    productDiv.appendChild(addToCartBtn);

    section.appendChild(productDiv);
}

// Evento que se dispara cuando el contenido HTML ha sido cargado
window.addEventListener('DOMContentLoaded', (event) => {
    mostrarProductosHTML();
});